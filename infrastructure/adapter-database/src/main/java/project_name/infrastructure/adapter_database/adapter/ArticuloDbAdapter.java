package project_name.infrastructure.adapter_database.adapter;

import project_name.domain.model.ArticuloDOMAIN;
import project_name.domain.ports.infrastructure.DatabasePort;
import project_name.infrastructure.adapter_database.mapper.ArticulosDbMapper;
import project_name.infrastructure.adapter_database.model.ArticuloAM;
import project_name.infrastructure.adapter_database.repositories.ArticulosREPOSITORY;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ArticuloDbAdapter implements DatabasePort {

    private final ArticulosREPOSITORY databaseRepository;

    private final ArticulosDbMapper mapper = ArticulosDbMapper.INSTANCE;

    @Override
    public ArticuloDOMAIN getArticuloByName(String name) {

        System.out.println("Llamada recibida ADAPTADOR DE INFRAESTRUCTURA DE BASE DE DATOS al método getArticulo, con valor: " + name);

        System.out.println("El ADAPTADOR DE INFRAESTRUCTURA DE BASE DE DATOS inserta artículo de ejemplo con el nombre q ha indicado el usuario");
        ArticuloAM clinex = new ArticuloAM(1234L, name,"88888321","Paquete de clinex");
        databaseRepository.save(clinex);

        ArticuloAM articuloMO = databaseRepository.getArticulo(name);

        System.out.println("El ADAPTADOR DE INFRAESTRUCTURA DE BASE DE DATOS devuelve los datos recibidos");

        return mapper.aDominio(articuloMO);
    }
}
