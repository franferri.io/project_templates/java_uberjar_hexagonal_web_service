package project_name.infrastructure.adapter_database.mapper;


import project_name.domain.model.ArticuloDOMAIN;
import project_name.infrastructure.adapter_database.model.ArticuloAM;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ArticulosDbMapper {

    ArticulosDbMapper INSTANCE = Mappers.getMapper(ArticulosDbMapper.class);


    @Mapping(source = "eanMerca", target = "ean13")
    ArticuloDOMAIN aDominio(ArticuloAM articuloMO);

    ArticuloAM aMO(ArticuloDOMAIN articulo);

}
