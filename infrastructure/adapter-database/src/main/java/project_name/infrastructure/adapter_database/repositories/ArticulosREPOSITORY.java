package project_name.infrastructure.adapter_database.repositories;


import project_name.infrastructure.adapter_database.model.ArticuloAM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticulosREPOSITORY extends JpaRepository<ArticuloAM, Integer> {

    @Query(value = "SELECT * FROM ARTICULOS WHERE nombre=:name", nativeQuery = true)
    ArticuloAM getArticulo(String name);

}
