package project_name.infrastructure.adapter_database.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Entity
@Table(name = "ARTICULOS")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ArticuloAM {

    @Id
    private Long id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "eanmerca")
    private String eanMerca;

    @Column(name = "description")
    private String description;

}
