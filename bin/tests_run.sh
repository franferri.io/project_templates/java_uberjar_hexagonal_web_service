#!/bin/bash

source "${BASH_SOURCE%/*}/libs/env"

cd "$SCRIPT_PATH" || exit 1
cd ..

# Run
############

./mvnw clean test -T 1C

exit 0
