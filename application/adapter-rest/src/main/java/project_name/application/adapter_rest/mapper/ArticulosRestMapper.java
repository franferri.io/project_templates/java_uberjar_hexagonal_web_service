package project_name.application.adapter_rest.mapper;

import project_name.application.adapter_rest.model.ArticuloAM;
import project_name.domain.model.ArticuloDOMAIN;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ArticulosRestMapper {

    ArticulosRestMapper INSTANCE = Mappers.getMapper(ArticulosRestMapper.class);

    ArticuloDOMAIN aDominio(ArticuloAM articuloREST);

    @Mapping(source = "ean13", target = "ean")
    ArticuloAM aREST(ArticuloDOMAIN articulo);

}
