package project_name.application.adapter_rest.api;

import project_name.application.adapter_rest.model.ArticuloAM;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/v1")
public interface ArticulosAPI {

    @GetMapping("/articulos/{name}")
    ArticuloAM getArticulo(@PathVariable String name);

}
