package project_name.application.adapter_rest.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ArticuloAM {

    private final String name;
    private final String ean;

}
