package project_name.application.adapter_web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ArticuloREST {

    private final String name;
    private final String ean;

}
